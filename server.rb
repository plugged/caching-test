require 'sinatra'

MAX_AGE = 2 * 60
NOW = Time.now 

configure do
 mime_type :js, 'text/javascript'
end

helpers do
  def time_for(value)
    case value
    when :past  then NOW - (NOW.to_i % MAX_AGE)
    when :future then NOW + MAX_AGE
    else super
    end
  end

  def request_headers
    env.inject({}){|acc, (k,v)| acc[$1] = v if k =~ /^http_(.*)/i; acc}
  end 
end

before do
  puts "REQUEST FOR #{request.path}"
  request_headers.each {|key, value| puts "  #{key}: #{value}" }
  puts
end

get '/caching/:type/random.js' do |type|
  case type.downcase
  when 'expires'
    expires :future
    cache_control ''
  when 'max-age'
    cache_control :private, :max_age => MAX_AGE
  when 'etag'
    etag (NOW.to_i / 30).to_s(16) # expire every 30 seconds
  when 'last-modified'
    last_modified :past
    #cache_control :public, :must_revalidate
    #cache_control :private, :must_revalidate, :max_age => 0
  end

  content_type :js
  "window.currentTime = '#{NOW.strftime '%H:%M:%S.%L'}';"
end

get '/caching/:type/' do 
  cache_control :private, :max_age => 0
  send_file File.expand_path('../index.html', __FILE__)
end

